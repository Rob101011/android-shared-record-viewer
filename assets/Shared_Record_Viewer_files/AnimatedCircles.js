var AnimatedCircles = function (circleContainerID) {
    this.circleContainerID = circleContainerID;
    this.$circle1 = null;
    this.$circle2 = null;
    this.$circle3 = null;

    this.executeAnimation = false;

    this.Init = function () {
        var instance = this;

        $(document).ready(function () {
            instance.$circle1 = $('#' + instance.circleContainerID + ' .circle-1');
            instance.$circle2 = $('#' + instance.circleContainerID + ' .circle-2');
            instance.$circle3 = $('#' + instance.circleContainerID + ' .circle-3');
        });
    };

    this.animateObjects = function () {
        var instance = this;
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle3);
        }, 400);
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle2);
        }, 800);
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle1);
        }, 1200);
    };

    this.fadeOutObject = function (object) {
        var instance = this;
        object.fadeOut({
            duration: 1000,
            complete: function () {
                instance.fadeInObject(object);
            }
        });
    };

    this.fadeInObject = function (object) {
        var instance = this;
        object.fadeIn({
            duration: 1000,
            complete: function () {
                if(instance.executeAnimation) {
                    instance.fadeOutObject(object);
                }
            }
        });
    };

    this.startAnimation = function () {
        this.executeAnimation = true;
        this.animateObjects();
    };

    this.stopAnimation = function () {
        this.executeAnimation = false;
    };
};