var AnimatedLoadingText = function (textContainerID, options) {
    this.textContainerID = textContainerID;

    this.options = options || {};

    this.loadingTextValue = this.options.loadingTextValue || 'Loading';
    this.circleSize = options.circleSize || 'USE_CSS';
    this.executeAnimation = false;

    this.$circle1 = null;
    this.$circle2 = null;
    this.$circle3 = null;

    this.Init = function () {
        var instance = this;
        $(document).ready(function () {
            instance.addLoadingElements();
            instance.$circle1 = $('#' + instance.textContainerID + 'circle-1');
            instance.$circle2 = $('#' + instance.textContainerID + 'circle-2');
            instance.$circle3 = $('#' + instance.textContainerID + 'circle-3');
            //instance.startAnimation();
        });
    };

    this.addLoadingElements = function () {
        $('#' + this.textContainerID).html(this.createLoadingElements());
    };

    this.createLoadingElements = function () {
        var style = '';

        if(this.circleSize !== 'USE_CSS') {
            style = 'height: ' + this.circleSize + 'px; width: ' + this.circleSize + 'px;';
        }

        var loadingElements = '';
        loadingElements += '<div class="loading-elements-container">';
        loadingElements += '<div class="animated-loading-text">' + ((this.loadingTextValue !== 'NONE') ? this.loadingTextValue : '') + '</div>';
        loadingElements += '<div id="' + this.textContainerID + 'circle-1" class="animated-circle animated-circle-1" style="' + style + '"></div>';
        loadingElements += '<div id="' + this.textContainerID + 'circle-2" class="animated-circle animated-circle-2" style="' + style + '"></div>';
        loadingElements += '<div id="' + this.textContainerID + 'circle-3" class="animated-circle animated-circle-3" style="' + style + '"></div>';
        loadingElements += '</div>';
        return loadingElements;
    };

    this.animateObjects = function () {
        var instance = this;
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle1);
        }, 400);
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle2);
        }, 800);
        setTimeout(function () {
            instance.fadeOutObject(instance.$circle3);
        }, 1200);
    };

    this.fadeOutObject = function (object) {
        var instance = this;
        object.fadeOut({
            duration: 1000,
            complete: function () {
                instance.fadeInObject(object);
            }
        });
    };

    this.fadeInObject = function (object) {
        var instance = this;
        object.fadeIn({
            duration: 1000,
            complete: function () {
                if(instance.executeAnimation) {
                    instance.fadeOutObject(object);
                }
            }
        });
    };

    this.startAnimation = function () {
        this.executeAnimation = true;
        this.animateObjects();
    };

    this.stopAnimation = function () {
        this.executeAnimation = false;
    };
};