// BirthdayPicker Plugin.

// Constructor
// textFieldID: is the id of the text field that needs the datepicker.
var BirthdayPickerPlugin = function (textFieldID, options) {
    this.options = options || {};

    this.$parentContainer = $(this);
    this.textfieldID = textFieldID;
    this.textfield = $('#' + textFieldID);
    this.pickerName = textFieldID + '-picker';

    this.yearFieldID    = this.pickerName + '-year-field';
    this.monthFieldID   = this.pickerName + '-month-field';
    this.dayFieldID     = this.pickerName + '-day-field';

    this.defaultDayValue = 'Day';
    this.defaultMonthValue = 'Month';
    this.defaultYearValue = 'Year';

    this.fieldClassName = 'birthday-picker-field';

    this.NUM_YEARS = 180;

    // Callbacks
    this.successfulValidationCallback = this.options.successCallback;
    this.failedValidationCallback = this.options.failureCallback;
    this.defaultValidationCallback = this.options.defaultCallback;
    this.dateFormatDelimiters = this.options.dateFormatDelimiters || '.';
    this.OnResetAllFields = this.options.OnResetAllFields;

    // Functions.

    ///////////
    // Events.
    ///////////
    this.SetupEventListeners = function () {
        var instance = this;

        this.textfield.on('focusin', function () {
            instance.OnTextFieldFocusIn();
        });

        this.textfield.on('focusout', function () {
            instance.OnTextFieldFocusOut();
        });

        this.textfield.on('click', function () {
            instance.OnTextFieldFocusIn();
        });

        this.textfield.on('change', function () {
            instance.OnTextFieldChanged();
        });

        $('body').on('change', '#' + this.yearFieldID, function () {
            instance.OnBirthdayFieldChanged();
        }).on('change', '#' + this.monthFieldID, function () {
            instance.OnBirthdayFieldChanged();
        }).on('change', '.' + this.fieldClassName, function () {
            instance.OnBirthdayFieldChanged();
        }).on('focusout', '.' + this.fieldClassName, function () {
            instance.OnDateOfBirthFieldFocusOut();
        });
    };

    this.OnTextFieldFocusIn = function () {
        this.showBirthdayPickerSlide();
    };

    this.OnTextFieldFocusOut = function () {
        // this.hideBirthdayPickerSlide();
        var instance = this;
        setTimeout(function() {
            var $focused = $(':focus');
            if(!$focused.hasClass(instance.fieldClassName)) {
                instance.hideBirthdayPickerSlide();
            }
        }, 100);
    };

    this.OnTextFieldChanged = function () {
        if(!this.isTextFieldEmpty()) {
            this.setDateFields(this.parseDateField());
        }
        else {
            this.resetAllDateFields();
            this.defaultValidationCallback();
        }
    };

    this.OnBirthdayFieldChanged = function () {
        this.validateBirthdayDropDownFields();
    };

    this.OnDateOfBirthFieldFocusOut = function () {
        var instance = this;
        setTimeout(function() {
            var $focused = $(':focus');

            if( $focused.attr('id') != instance.textfieldID &&
                !$focused.hasClass(instance.fieldClassName)) {
                instance.hideBirthdayPickerSlide();
            }
        }, 100);
    };

    // Picker functions.
    this.createBirthdayPicker = function () {
        this.$parentContainer.append(this.buildBirthdayPicker());
    };

    this.buildBirthdayPicker = function () {
        var pickerHTML = '';

        pickerHTML += this.buildYears();
        pickerHTML += this.buildMonths();
        pickerHTML += this.buildDays();

        return pickerHTML;
    };

    this.buildDays = function () {
        var days = '<select id="' + this.dayFieldID + '" class="' + this.fieldClassName + '">';
        days += '<option>Day</option>';

        return days;
    };

    this.updateDays = function () {
        var selectedYear = $('#' + this.yearFieldID).val();
        var selectedMonth = $('#' + this.monthFieldID).val();

        if( selectedMonth === this.defaultMonthValue ||
            selectedYear === this.defaultYearValue) {
            return;
        }

        var numDays = new Date(selectedYear, selectedMonth, 0).getDate();

        var html = '<option>' + this.defaultDayValue + '</option>';

        for(var i = 1; i <= numDays; i++) {
            html += '<option>' + i + '</option>';
        }

        this.enableDayField();
        $('#' + this.dayFieldID).html(html);
    };

    this.buildMonths = function () {
        var NUM_MONTHS = 12;
        var monthStrings = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

        var months = '<select id="' + this.monthFieldID + '" class="' + this.fieldClassName + '">';
        months += '<option value="' + this.defaultMonthValue + '">' + this.defaultMonthValue + '</option>';

        for(var i = 0; i < NUM_MONTHS; i++){
            months += '<option value="' + (i + 1) + '">' + monthStrings[i] + '</option>'
        }
        months += '</select>';

        return months;
    };

    this.buildYears = function () {

        var currentYear = new Date().getFullYear();

        var years = '<select id="' + this.yearFieldID + '" class="' + this.fieldClassName + '">';
        years += '<option>' + this.defaultYearValue + '</option>';

        for(var i = 0; i < this.NUM_YEARS; i++) {
            years += '<option>' + (currentYear - i) + '</option>';
        }
        years += '</select>';

        return years;
    };

    this.validateBirthdayDropDownFields = function () {

        var selectedYear = this.getYearFieldValue();
        var selectedMonth = this.getMonthFieldValue();
        var selectedDay = this.getDayFieldValue();

        if( selectedMonth === this.defaultMonthValue ||
            selectedYear === this.defaultYearValue) {
            this.resetDayField();
            this.disableDayField();
        }
        else if(selectedMonth != this.defaultMonthValue &&
                selectedYear != this.defaultYearValue &&
                selectedDay === this.defaultDayValue) {
            this.updateDays();
        }
        if(selectedDay === this.defaultDayValue ||
            selectedMonth === this.defaultMonthValue ||
            selectedYear === this.defaultYearValue) {
            this.resetDateTextField();
            this.failedValidationCallback();
        }
        else {
            this.textfield.val(this.formatDate(selectedDay, selectedMonth, selectedYear));

            this.successfulValidationCallback(this.formatDate(selectedDay, selectedMonth, selectedYear));
            this.OnTextFieldChanged();
        }
    };

    this.formatDate = function (day, month, year) {
        return day + this.dateFormatDelimiters[0] + month + this.dateFormatDelimiters[0] + year;
    };

    this.createDateObject = function (day, month, year) {
        var dateObj = {};
        dateObj.day = day;
        dateObj.month = month;
        dateObj.year = year;

        return dateObj;
    }

    this.parseDateField = function () {
        var textfieldVal = this.textfield.val();

        var splitDateObj = this.parseDateWithDelimiters(textfieldVal);

        return splitDateObj;
    };

    this.parseDateWithDelimiters = function (dateStr) {
        for(var i = 0; i < this.dateFormatDelimiters.length; i++) {
            var splitDate = {};
            splitDate.day = parseInt(dateStr.split(this.dateFormatDelimiters[i])[0]);
            splitDate.month = parseInt(dateStr.split(this.dateFormatDelimiters[i])[1]);
            splitDate.year = parseInt(dateStr.split(this.dateFormatDelimiters[i])[2]);

            if(splitDate.day != null && splitDate.month != null && splitDate.year != null) {
                if(!isNaN(splitDate.day) && !isNaN(splitDate.month) && !isNaN(splitDate.year)) {
                    return splitDate;
                }
            }
        }
    };

    this.setDateFields = function (splitDateObj) {

        if(this.validateAllDateFields(splitDateObj)) {

            $('#' + this.yearFieldID).val(splitDateObj.year);
            $('#' + this.monthFieldID).val(splitDateObj.month);

            this.enableDayField();
            this.updateDays();

            $('#' + this.dayFieldID).val(splitDateObj.day);

            if(!this.validateAllDateFields()) {
                this.resetAllDateFields();
                this.resetDateTextField();
                this.failedValidationCallback();
                return;
            }
        }
        else {
            this.resetAllDateFields();
            this.resetDateTextField();
            this.failedValidationCallback();
            return;
        }

        var selectedYear = this.getYearFieldValue();
        var selectedMonth = this.getMonthFieldValue();
        var selectedDay = this.getDayFieldValue();

        this.successfulValidationCallback(this.formatDate(selectedDay, selectedMonth, selectedYear));
    };

    this.validateAllDateFields = function (splitDateObj) {
        if(splitDateObj != null) {
            return (
                this.valdateYearField(splitDateObj.year) &&
                this.validateMonthField(splitDateObj.month) &&
                this.validateDayField(splitDateObj.day, splitDateObj.month, splitDateObj.year)
            );
        }
        else {
            return (
            this.valdateYearField(this.getYearFieldValue()) &&
            this.validateMonthField(this.getMonthFieldValue()) &&
            this.validateDayField(this.getDayFieldValue(), this.getMonthFieldValue(), this.getYearFieldValue())
            );
        }
    };

    this.validateDayField = function (dayFieldValue, monthFieldValue, yearFieldValue) {

        var maxDays = new Date(yearFieldValue, monthFieldValue, 0).getDate();

        var todaysDate = new Date();
        todaysDate.setHours(0, 0, 0, 0);

        var currentDate = new Date(yearFieldValue, monthFieldValue, dayFieldValue);

        // Are we in the future?!?
        if( currentDate > todaysDate &&
            currentDate.getDate() > todaysDate.getDate()) {
            this.resetDayFieldValue();
            return false;
        }

        if(isNaN(dayFieldValue)) {
            this.resetDayFieldValue();
            return false;
        }

        if(parseInt(dayFieldValue) > maxDays) {
            this.resetDayFieldValue();
            return false;
        }

        if(parseInt(dayFieldValue) <= 0) {
            this.resetDayFieldValue();
            return false;
        }

        return true;
    };

    this.validateMonthField = function (monthFieldValue) {

        var maxMonths = 12;

        if(isNaN(monthFieldValue)) {
            this.resetMonthFieldValue();
            return false;
        }

        if(parseInt(monthFieldValue) > maxMonths) {
            this.resetMonthFieldValue();
            return false;
        }

        if(parseInt(monthFieldValue) <= 0) {
            this.resetMonthFieldValue();
            return false;
        }

        return true;
    };

    this.valdateYearField = function (yearFieldValue) {

        var currentYear = new Date().getFullYear();
        var oldestYear = currentYear - this.NUM_YEARS;

        if(isNaN(yearFieldValue)) {
            this.resetYearFieldValue();
            return false;
        }

        if(parseInt(yearFieldValue) > currentYear || parseInt(yearFieldValue) < oldestYear) {
            this.resetYearFieldValue();
            return false;
        }

        if(parseInt(yearFieldValue) <= 0) {
            this.resetYearFieldValue();
            return false;
        }

        return true;
    };

    this.isTextFieldEmpty = function () {
        return this.textfield.val() === '';
    };

    this.showBirthdayPickerSlide = function () {
        this.$parentContainer.slideDown();
    };

    this.hideBirthdayPickerSlide = function () {

        this.$parentContainer.slideUp();
    };

    this.showBirthdayPicker = function () {
        this.$parentContainer.show();
    };

    this.hideBirthdayPicker = function () {
        this.$parentContainer.hide();
    };

    this.disableDayField = function () {
        $('#' + this.dayFieldID).attr('disabled', true);
    };

    this.enableDayField = function () {
        $('#' + this.dayFieldID).removeAttr('disabled');
    };

    this.resetDayField = function () {
        $('#' + this.dayFieldID).html('<option>' + this.defaultDayValue + '</option>');
    };

    this.resetDayFieldValue = function () {
        $('#' + this.dayFieldID).val(this.defaultDayValue);
        this.disableDayField();
    };

    this.resetMonthFieldValue = function () {
        $('#' + this.monthFieldID).val(this.defaultMonthValue);
    };

    this.resetYearFieldValue = function () {
        $('#' + this.yearFieldID).val(this.defaultYearValue);
    };

    this.resetDateTextField = function () {
        this.textfield.val('');
    };

    this.resetAllDateFields = function () {
        this.resetDayFieldValue();
        this.resetMonthFieldValue();
        this.resetYearFieldValue();
    };

    this.getDayFieldValue = function () {
        return $('#' + this.dayFieldID).val();
    };

    this.getMonthFieldValue = function () {
        return $('#' + this.monthFieldID).val();
    };

    this.getYearFieldValue = function () {
        return $('#' + this.yearFieldID).val();
    };

    this.getDotSeparatedDate = function (day, month, year) {
        return day + '.' + month + '.' + year;
    };

    // Setup.
    this.SetupEventListeners();
    this.createBirthdayPicker();
    this.disableDayField();
    this.hideBirthdayPicker();

    return this;
};

// Jquery function.
$.fn.birthdayPicker = BirthdayPickerPlugin;