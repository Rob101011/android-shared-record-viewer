var HcgwModal = function (options) {
    this.options = options || {};
    this.modalID = options.modalID || '';
    this.modalTitle = options.modalTitle || 'Modal title';
    this.containerID = options.containerID || '';
    this.hidden = options.hidden || false;
    this.okButtonLabel = options.okButtonLabel || 'Ok';
    this.cancelButtonLabel = options.cancelButtonLabel || 'Cancel';
    this.bodyHTML = options.bodyHTML || '';
    this.numButtons = options.numButtons || 2;
    this.transitionIn = options.transitionIn || false;

    this.okButtonCallback = options.okButtonCallback || this.defaultOKCallback;
    this.cancelButtonCallback = options.cancelButtonCallback || this.defaultCancelButtonCallback;

    this.createNewModal = function () {
        var instance = this;

        this.containerID = this.modalID + '-container';

        var modalHTML = this.generateModalHTML();

        if($('#' + this.containerID).length) {
            $('#' + this.containerID).replaceWith(modalHTML);
        } else {
            $('body').append(modalHTML);
        }

        if(this.hidden) {
            this.hideModal();
        }

        // Setup listeners.
        $('#' + this.modalID + '-ok-button').on('click', function () {
            instance.okButtonCallback();
        });

        $('#' + this.modalID + '-cancel-button').on('click', function () {
            instance.cancelButtonCallback();
        });
    };

    this.generateModalHTML = function () {
        var modalHtml = '';
        modalHtml += '<div id="' + this.containerID + '">';
        modalHtml += '<div id="' + this.modalID + '-backdrop" class="hcgw-modal-backdrop"></div>';
        modalHtml += '<div id="' + this.modalID + '" class="hcgw-modal">';
        modalHtml += '<div class="hcgw-modal-innner-container">';
        modalHtml += '<div class="hcgw-modal-header">';
        modalHtml += '<h1>' + this.modalTitle + '</h1>';
        modalHtml += '<div class="hcgw-stripe-top"></div>';
        modalHtml += '<div class="hcgw-stripe-bottom"></div>';
        modalHtml += '<div class="hcgw-circle hcgw-circle-1"></div>';
        modalHtml += '<div class="hcgw-circle hcgw-circle-2"></div>';
        modalHtml += '<div class="hcgw-circle hcgw-circle-3"></div>';
        modalHtml += '</div>';
        modalHtml += '<div class="hcgw-modal-body">' + this.bodyHTML + '</div>';
        modalHtml += '<div class="hcgw-modal-footer">';
        modalHtml += '<button id="' + this.modalID + '-ok-button" class="hcgw-btn hcgw-btn-confirm">' + this.okButtonLabel + '</button>';
        if(this.numButtons > 1) {
            modalHtml += '<button id="' + this.modalID + '-cancel-button" class="hcgw-btn hcgw-btn-neutral">' + this.cancelButtonLabel + '</button>';
        }
        modalHtml += '</div>';
        modalHtml += '</div>';
        modalHtml += '</div>';

        modalHtml += '</div>';
        return modalHtml;
    };

    this.hideModal = function () {
        $('#' + this.containerID).hide();
        if(this.transitionIn) {
            $('#' + this.modalID).animate({top: '-300px'}, 0);
        }
    };

    this.showModal = function () {
        $('#' + this.containerID).show();
        if(this.transitionIn) {
            $('#' + this.modalID).animate({top: '0'}, 500);
        }
    };

    // Default callbacks
    this.defaultOKCallback = function () {
        this.hideModal();
    };

    this.defaultCancelButtonCallback = function () {
        this.showModal();
    };

    this.createNewModal();
};