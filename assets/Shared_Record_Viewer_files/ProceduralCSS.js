var ProceduralCircles = function (circleContainerID, options) {
    this.cssClasses = [];
    this.cssColors = options.colors || ['#354F00', '#7B9F35', '#869566', '#D4EE9F'];
    this.bias = options.bias || 'none';
    this.numCirlces = options.numCirlces || 50;
    this.circleContainerID = circleContainerID;
    this.spawnType = options.spawnType || 'all';
    this.spawnSpeed = options.spawnSpeed || 100;
    this.width = 0;
    this.height = 0;

    this.currentCircles = 0;

    this.circleIDQueue = [];

    this.Init = function () {
        this.insertCircles();
    };

    this.insertCircles = function () {
        var instance = this;

        $(document).ready(function () {
            instance.width = $('#' + instance.circleContainerID).width();
            instance.height = $('#' + instance.circleContainerID).height();

            if(instance.spawnType === 'all') {
                instance.spawnAll();
            } else if(instance.spawnType === 'oneByOne') {
                instance.spawnOneByOne();
            }
        });
    };
    this.spawnOneByOne = function () {
        var instance = this;
        setTimeout(function () {
            var id = 'circle_' + this.currentCircles
            $('#' + instance.circleContainerID).append(instance.createCircle());
            if(instance.currentCircles != instance.numCirlces) {
                instance.spawnOneByOne();
            }
            instance.currentCircles++;
        },  this.spawnSpeed);
    };

    this.spawnAll = function () {
        for(var i = 0; i < this.numCirlces; i++) {
            $('#' + this.circleContainerID).append(this.createCircle());
        }
    };

    this.createCircle = function () {
        var circle = '';
        circle += '<div id= "circle_' + this.currentCircles + '" style="' + this.createClass() + '"></div>';
        return circle;
    };

    this.createClass = function () {
        var size = Math.floor(Math.random() * 15) + 5;
        var pos = this.getRandomPosition();

        var cssClass = '';
        cssClass += 'background-color:' + this.getRandomHexColor() + ';';
        cssClass += 'border-radius:50%;';
        cssClass += 'height:' + size + 'px;';
        cssClass += 'position:absolute;';
        cssClass += 'right:' + pos.x + 'px;';
        cssClass += 'top: '+ pos.y + 'px;';
        cssClass += 'width:' + size + 'px;';
        cssClass += 'opacity: 0.7;';
        return cssClass;
    };

    this.getRandomHexColor = function () {
        var random = Math.floor((Math.random() * this.cssColors.length));
        return this.cssColors[random];
    };

    this.getRandomPosition = function () {
        var pos = {};
        pos.x = Math.floor(Math.random() * this.width) + this.getBiasXModifier();
        pos.y = Math.floor(Math.random() * this.height) + this.getBiasYModifier();
        return pos;
    };

    this.getBiasXModifier = function () {
        switch (this.bias) {
            case 'right':
                return -(this.width * 0.5);
            case 'left':
                return (this.width * 0.5);
            default:
                return 0;
        }
    };

    this.getBiasYModifier = function () {
        switch (this.bias) {
            case 'top':
                return -(this.height * 0.4);
            case 'bottom':
                return (this.height * 0.4);
            default:
                return 0;
        }
    };

    this.Init();
};