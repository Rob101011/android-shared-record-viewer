var GenericFieldValidator = function (type, options) {
    this.$_field = $(this);

    this.fieldType = type;
    this.options = options || {};
    this.OnValidationSuccessCallback = this.options.successCallback;
    this.OnValidationFailureCallback = this.options.failureCallback;
    this.OnValidationDefaultCallback = this.options.defaultCallback;

    this.MAX_CHARACTERS = this.options.maxCharacters || 50;
    this.MAX_NUM_CHARACTERS = this.options.maxNumCharacters || 50;
    this.BANNED_FIELD_VALUES = this.options.bannedFieldValues || [];
    this.RESULTANT_FORMAT = this.options.resultantFormat || 'none';

    this.FIELD_TYPE_TEXT = 'text-field';
    this.FIELD_TYPE_NON_NUMERIC_TEXT = 'non-numeric-text-field';
    this.FIELD_TYPE_NUMERIC = 'numeric-text-field';
    this.FIELD_TYPE_POST_CODE = 'postcode-field';
    this.FIELD_TYPE_DATE = 'date-field';
    this.FIELD_TYPE_DROPDOWN = 'dropdown-field';
    this.FIELD_TYPE_EMAIL_ADDRESS = 'email-field';

    this.fieldValidator = null;

    this.acceptBlank = this.options.acceptBlank || false;

    this.BANNED_CHARACTERS = [
            '\\', '|', '"', '£', '$', '%', '^', '&',
            '*', '<', '>', '/', '?', ',', '.', '#',
            '@', ';', ':', '[', ']', '{', '}', '_',
            '+', '=', '(', ')'];

    this.MAX_POSTCODE_LENGTH = 8;

    this.FAILURE_REASONS = {};
    this.FAILURE_REASONS.length = "length";

    ///////////
    // Setup.
    ///////////

    this.Setup = function () {
        this.setupEvents();
        this.setupValidator();

        if(this.acceptBlank) {
            this.OnFieldValidationBlank();
        } else if(this.fieldValidator == this.validateDropdown) {
            this.fieldValidator(this.getFieldValue());
        }
    };

    // Chooses appropriate function to validate
    // field of the given type.
    this.setupValidator = function () {
        if(this.fieldType === this.FIELD_TYPE_TEXT) {
            this.fieldValidator = this.validateText;
        }
        else if(this.fieldType === this.FIELD_TYPE_NON_NUMERIC_TEXT) {
            this.fieldValidator = this.validateNonNumericText;
        }
        else if(this.fieldType === this.FIELD_TYPE_NUMERIC) {
            this.fieldValidator = this.validateNumericText;
        }
        else if(this.fieldType === this.FIELD_TYPE_POST_CODE) {
            this.fieldValidator = this.validatePostCode;
        }
        else if(this.fieldType === this.FIELD_TYPE_DATE) {
            this.fieldValidator = this.validateDate;
        }
        else if(this.fieldType === this.FIELD_TYPE_DROPDOWN) {
            this.fieldValidator = this.validateDropdown;
        }
        else if(this.fieldType === this.FIELD_TYPE_EMAIL_ADDRESS) {
            this.fieldValidator = this.validateEmailAddress;
        }
    };

    ////////////
    // Events.
    ////////////

    this.setupEvents = function () {
        var instance = this;

        this.$_field.on('change', function () {
            instance.OnFieldChanged();
        }).on('keydown', function (e) {
            instance.OnKeydown(e);
        });
    };

    this.OnFieldChanged = function () {
        this.fieldValidator(this.getFieldValue());
    };

    this.OnFieldValidationSuccess = function (fieldValue) {

        this.$_field.data('isFieldValid', true);
        if(this.OnValidationSuccessCallback != null) {
            this.OnValidationSuccessCallback(fieldValue || this.getFieldValue());
        }
    };

    this.OnFieldValidationFailure = function (reason) {

        this.$_field.data('isFieldValid', false);

        if(this.OnValidationFailureCallback != null) {
            this.OnValidationFailureCallback(reason);
        }
    };

    this.OnFieldValidationBlank = function () {

        this.$_field.data('isFieldValid', this.acceptBlank);

        if(this.OnValidationDefaultCallback != null) {
            this.OnValidationDefaultCallback(this.getFieldValue());
        }
    };

    this.OnKeydown = function (e) {
        if (e.keyCode === 13) {
            this.OnEnterButtonPressed();
        }
    };

    this.OnEnterButtonPressed = function () {
        this.OnFieldChanged();
    };

    ////////////////////////
    // Field manipulation.
    ////////////////////////

    this.getFieldValue = function () {
        return this.$_field.val();
    };

    this.resetFieldValue = function () {
        this.$_field.val('');
    }

    this.setFieldValue = function (fieldValue) {
        this.$_field.val(fieldValue);
    };

    // Validation.

    this.validateStandard = function (fieldValue) {
        // Check if blank.
        if (fieldValue === '') {
            this.OnFieldValidationBlank();
            return false;
        }

        // Check that our field is does not have more than
        // the maximum allowed characters.
        if (fieldValue.length > this.MAX_CHARACTERS) {
            return false;
        }

        // Check if the value is made up of nothing
        // but white space.
        var noSpaceString = fieldValue.replace(/\s+/g, '');

        if (noSpaceString.length === 0) {
            this.resetFieldValue();
            this.OnFieldValidationBlank();
            return false;
        }

        return true;
    };

    this.validateText = function (fieldValue) {
        // Remove banned characters from string.
        var striped = this.removeBannedCharactersFromString(fieldValue);
        striped = StringHelper.trim(striped);

        if(this.checkStringForBannedChars(striped)) {
            return false;
        }

        if(!this.validateStandard(striped)) {
            this.OnFieldValidationFailure();
            return false;
        }

        this.OnFieldValidationSuccess(striped);

        return true;
    };

    this.validateNonNumericText = function (fieldValue) {

        // Do standard validation.
        if(!this.validateStandard(fieldValue)) {
            return false;
        }

        // Strip out the numbers but keep the spaces.
        var noNumberString = '';
        for (var i = 0; i < fieldValue.length; i++) {
            if (isNaN(fieldValue.charAt(i)) &&
                !this.checkCharForBannedChars(fieldValue.charAt(i))) {
                noNumberString += fieldValue.charAt(i);
            }
            else if (fieldValue.charAt(i) === ' ') {
                noNumberString += ' ';
            }
        }

        // Check that we still have a length.
        if (noNumberString.length === 0) {
            this.resetFieldValue();
            this.OnFieldValidationBlank();
            return false;
        }

        noNumberString = StringHelper.trim(noNumberString);

        // Update the field.
        this.setFieldValue(this.formatValue(noNumberString));

        this.OnFieldValidationSuccess(noNumberString);

        return true;
    };

    this.validateNumericText = function (fieldValue) {

        if(!this.validateStandard(fieldValue)) {
            return false;
        }

        var validatedNum = '';

        // Remove non-numbers.
        for(var i = 0; i < fieldValue.length; i++) {
            var numChar = parseInt(fieldValue.charAt(i));
            if(!isNaN(numChar)) {
                validatedNum += numChar;
            }
        }

        if(validatedNum.length === this.MAX_NUM_CHARACTERS) {

            this.setFieldValue(this.formatValue(validatedNum));

            this.OnFieldValidationSuccess(validatedNum);
            return true;
        }
        else {
            this.OnFieldValidationFailure();
            return false;
        }

    };

    this.validatePostCode = function (fieldValue) {
        if(!this.validateStandard(fieldValue)) {
            return false;
        }

        var strippedStr = '';
        fieldValue = StringHelper.trim(fieldValue);

        // Remove banned chars and change to upper case.
        for(var i = 0; i < fieldValue.length; i++) {
            if(!this.checkCharForBannedChars(fieldValue.charAt(i))) {
                strippedStr += fieldValue.charAt(i).toUpperCase();
            }
        }

        // Remove spaces.
        strippedStr = strippedStr.replace(/\s+/g, '');

        if(fieldValue.length > this.MAX_POSTCODE_LENGTH) {
            this.OnFieldValidationFailure();
            return false;
        }

        // Re-add spaces to format like 'DD1 3BN'.
        // http://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting
        var last = strippedStr.length - 1;
        var spaceIndex = last - 2;
        var formatted = strippedStr.slice(0, spaceIndex) + " " + strippedStr.slice(spaceIndex);

        // Add correctly formatted post code to field.
        this.setFieldValue(formatted);
        this.OnFieldValidationSuccess(formatted);

        return true;
    };

    this.validateDate = function (fieldValue) {
        // TODO: Implement this.
        // MP: This is currently not implemented because we
        // are using another plugin to handle the
        // date validation for the current project.
        alert("Error! Not implemented, See comment!");
    };

    this.validateEmailAddress = function (fieldValue) {
        if(!this.validateStandard(fieldValue)) {
            this.OnFieldValidationFailure();
            return false;
        }

        fieldValue = StringHelper.trim(fieldValue);

        var emailRegex = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$';
        var pattern = new RegExp(emailRegex);

        if(!pattern.test(fieldValue)) {
            this.OnFieldValidationFailure();
        }

        this.OnFieldValidationSuccess(fieldValue);

        return true;
    };

    this.validateDropdown = function (fieldValue) {

        if(!this.acceptBlank && fieldValue == null) {
            this.OnFieldValidationFailure();
            return false;
        }

        if(!this.checkForBannedFieldValues(fieldValue)) {
            this.OnFieldValidationFailure();
            return false;
        }
        this.OnFieldValidationSuccess(fieldValue);
        return true;
    },

    this.checkCharForBannedChars = function (char) {
        for(var i = 0; i < this.BANNED_CHARACTERS.length; i++) {
            if(char == this.BANNED_CHARACTERS[i]) {
                return true;
            }
        }
        return false;
    };

    this.checkStringForBannedChars = function (string) {
        for(var i = 0; i <this.BANNED_CHARACTERS.length; i++) {
            for(var j = 0; j < string.length; j++) {
                if(string[j] === this.BANNED_CHARACTERS[i]) {
                    return true;
                }
            }
        }
        return false;
    };

    this.checkForBannedFieldValues = function (fieldValue) {
        for(var i = 0; i < this.BANNED_FIELD_VALUES.length; i++) {
            if(fieldValue === this.BANNED_FIELD_VALUES[i]) {
                return false;
            }
        }
        return true;
    };

    this.removeBannedCharactersFromString = function(string) {
        var stripedString = '';
        for(var i = 0; i < string.length; i++) {
            if(!this.checkStringForBannedChars(string.charAt(i))) {
                stripedString += string.charAt(i);
            }
        }
        return stripedString;
    };

    this.formatValue = function (value) {
        var formatDelimiter = '-';
        var formatChar = 'x';
        var formattedValue = '';

        if(this.RESULTANT_FORMAT != 'none') {
            var valueIndex = 0;

            for (var i = 0; i < this.RESULTANT_FORMAT.length; i++) {
                if(this.RESULTANT_FORMAT.charAt(i) === formatChar && value.charAt(valueIndex) != null) {
                    formattedValue += value.charAt(valueIndex);
                    valueIndex++;
                }
                else if(this.RESULTANT_FORMAT.charAt(i) === formatDelimiter) {
                    formattedValue += formatDelimiter;
                }
            }

            return (formattedValue !== '') ? formattedValue : value;
        }
        else {
            return value;
        }
    };

    this.Setup();
};

$.fn.validateField = GenericFieldValidator;