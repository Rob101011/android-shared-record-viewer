var StringHelper = StringHelper || {

    firstLetterToUpper: function (inputStr) {
        var outputStr = inputStr.toLowerCase();
        return outputStr.charAt(0).toUpperCase() + outputStr.slice(1);
    },
    trim: function (inputStr) {
        return inputStr.replace(/^\s+|\s+$/g, '');
    },
    toUpperCase: function () {

    }
};

var CSSHelper = CSSHelper || {
    getCSSAsString: function (cssFileName) {
        var sheets = document.styleSheets;
        var sheetNameRegX = new RegExp(cssFileName);
        var styleSheetStr = '';
        for(var i = 0; i < sheets.length; i++) {
            if(sheets[i].href != null) {
                if(sheetNameRegX.test(sheets[i].href)) {
                    if(sheets[i].cssRules != null) {
                        for(var j = 0; j < sheets[i].cssRules.length; j++) {
                            styleSheetStr += sheets[i].cssRules[j].cssText;
                        }
                    } else if (sheets[i].rules != null) {
                        for(var j = 0; j < sheets[i].rules.length; j++) {
                            styleSheetStr += sheets[i].rules[j].selectorText;
                            styleSheetStr += '{'
                            for(var key in sheets[i].rules[j].style) {
                                if(typeof sheets[i].rules[j].style.key !== undefined) {
                                    var obj = sheets[i].rules[j].style[key];
                                    if(obj !== '' && obj != '0') {
                                        styleSheetStr += key + ": " + obj + ';';
                                    }
                                }
                            }
                            styleSheetStr += '}';
                        }
                    }
                }
            }
        }
        return styleSheetStr;
    }
};

var DateHelper = DateHelper || {
    getCurrentDateNow: function () {
        Date.now = Date.now || function() { return +new Date; };
        return Date.now();
    }
};