/**
 * Created by Dongiel Radcliff on 02/04/2015.
 */

$.fn.animateRotate = function(angle, duration, easing, complete) {
    var args = $.speed(duration, easing, complete);
    var step = args.step;
    return this.each(function(i, e) {
        args.step = function(now) {
            $.style(e, 'transform', 'rotate(' + now + 'deg)');
            if (step) return step.apply(this, arguments);
        };
        if(angle !== -180) $({deg: 0}).animate({deg: angle}, args);
        else $({deg: -180}).animate({deg: 0}, args);
    });
};

(function($) {
    $.fn.onMouse = function (element, callback) {
        return this.each(function() {
            this.mousedownTime = 0;
            this.mousedown = false;
            this.currentMouseEvent = null;
            this.OnClickCallback = callback;
            this.clickedElement = null;

            var instance = this;

            $('body')
                .on('mousedown', element, function (event) {
                    switch(event.which) {
                        case 1:
                            instance.OnMouseButton1Down();
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                    }

                    instance.clickedElement = this;
                })
                .on('mouseup', element, function (event) {
                    switch(event.which) {
                        case 1:
                            instance.OnMouseButton1Up();
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                    }
                    instance.clickedElement = this;
                });

            this.OnMouseButton1Down = function () {
                if(this.currentMouseEvent == null) {
                    this.currentMouseEvent = new MouseEvent(this.OnMouseEventFinished, 'button1');
                } else {
                    if(this.currentMouseEvent.isDoubleClickTimeUp()) {
                        this.currentMouseEvent = new MouseEvent(this.OnMouseEventFinished, 'button1');
                    }
                }

                this.currentMouseEvent.OnMouseDown();
            };

            this.OnMouseButton1Up = function () {
                var instance = this;

                this.currentMouseEvent.OnMouseUp();
            };

            this.OnMouseEventFinished = function (clickType, btnName) {
                instance.OnClickCallback(clickType, btnName, instance.clickedElement);
            }
        });
    };
}(jQuery));

var MouseEvent = function (callback, btnName) {
    this.mousedownTime = 0;
    this.lastClickTime = 0;
    this.isMouseDown = false;
    this.numClicks = 0;
    this.DOUBLE_CLICK_TIME = 75;
    this.MAX_MOUSE_DOWN_TIME = 400;
    this.OnFinishCallback = callback;
    this.doubleTriggered = false;
    this.buttonName = btnName;

    this.OnMouseDown = function () {
        this.isMouseDown = true;
        this.timeMouseDown();
    };

    this.OnMouseUp = function () {
        this.isMouseDown = false;
        this.lastClickTime = 0;
        this.numClicks++;
        if(this.isDoubleClick()) {
            this.doubleTriggered = true;
            this.OnFinishCallback('double', this.buttonName);
        }
        this.timeSinceLastClick();
    };

    this.timeMouseDown = function () {
        var instance = this;
        setTimeout(function () {
            if(instance.isMouseDown) {
                instance.timeMouseDown();
            }
            instance.mousedownTime += 100;
        }, 100);
    };

    this.timeSinceLastClick = function () {
        var instance = this;

        setTimeout(function () {
            if(!instance.isMouseDown && !instance.isDoubleClickTimeUp()) {
                instance.timeSinceLastClick();
            } else if(instance.isDoubleClickTimeUp() && !instance.doubleTriggered) {
                if(instance.mousedownTime >= instance.MAX_MOUSE_DOWN_TIME) {

                } else {
                    instance.OnFinishCallback('single', instance.buttonName);
                }

            }
            instance.lastClickTime += 10;
        }, 10);
    };

    this.isDoubleClick = function () {
        if(this.lastClickTime < this.DOUBLE_CLICK_TIME) {

            if(this.numClicks === 2) {

                return true;
            }
        }
        return false;
    };

    this.isDoubleClickTimeUp = function () {
        if(this.lastClickTime < this.DOUBLE_CLICK_TIME) {
            return false;
        }
        return true;
    };
};

(function($) {
    $.fn.dropdownMenu = function (elementID, expandedID) {
        return this.each(function() {
            var instance = this;
            this.elementID = elementID;
            this.expandedID = expandedID;
            this.expanded = false;

            this.setupEventListeners = function () {
                $('#' + this.elementID).on('click', instance.OnMenuButtonPressed);
                $('#' + this.expandedID).hide();
            };

            this.OnMenuButtonPressed = function () {

                if(this.expanded) {
                    this.contractMenu();
                } else {
                    this.expandMenu();
                }
            };

            this.expandMenu = function () {
                this.expanded = true;
                $('#' + this.expandedID).slideDown();
            };

            this.contractMenu = function () {
                this.expanded = false;
                $('#' + this.expandedID).slideUp();
            };

            $(document).ready(function () {
                instance.setupEventListeners();
            });
        });
    };
}(jQuery));