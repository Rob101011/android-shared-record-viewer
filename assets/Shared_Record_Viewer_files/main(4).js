var HcgwDockbar = HcgwDockbar || {

    openMenu: null,
    mobileMenuOpen: false,

    Init: function () {
        $(document).ready(function () {
            HcgwDockbar.setupEventListeners();
        });
    },

    setupEventListeners: function () {
        $(document).on('click', function () {
            if(HcgwDockbar.openMenu != null) {
                HcgwDockbar.hideMenu(HcgwDockbar.openMenu)
            }

            if(HcgwDockbar.mobileMenuOpen) {
                HcgwDockbar.hideMobileMenu();
            }
        });

        $('.hcgw-dropdown').on('click', function (event) {
            event.stopPropagation();
            var id = $(this).attr('id');
            var childLi = $('#' + id + ' ul');
            HcgwDockbar.toggleMenu(childLi);
        });

        $('#hcgw-mobile-menu').on('click', function (event) {
            event.stopPropagation();
            HcgwDockbar.toggleMobileMenu();
        });

        $('#hcgw-user-panel-button').on('click', function (event) {
            $('#hcgw-user-panel-modal').modal('show');
        });
    },

    toggleMenu: function (menu) {
        if(menu.hasClass('hcgw-menu-closed')) {
            HcgwDockbar.showMenu(menu);
        } else if (menu.hasClass('hcgw-menu-open')) {
            HcgwDockbar.hideMenu(menu);
        }
    },

    showMenu: function (menu) {
        if(HcgwDockbar.openMenu != null) {
            HcgwDockbar.hideMenu(HcgwDockbar.openMenu);
        }

        menu.show();
        menu.removeClass('hcgw-menu-closed').addClass('hcgw-menu-open').addClass('selected-menu-element');
        menu.parent().addClass('selected-menu-element');

        HcgwDockbar.openMenu = menu;
    },

    hideMenu: function (menu) {
        menu.hide();
        menu.removeClass('hcgw-menu-open').removeClass('selected-menu-element').addClass('hcgw-menu-closed')
        menu.parent().removeClass('selected-menu-element');

        HcgwDockbar.openMenu = null;
    },

    toggleMobileMenu: function () {
        if(!HcgwDockbar.mobileMenuOpen) {
            HcgwDockbar.showMobileMenu();
        } else {
            HcgwDockbar.hideMobileMenu();
        }
    },

    showMobileMenu: function () {
        $('#hcgw-mobile-dropdown-menu').show();
        $('#hcgw-mobile-menu-icon').addClass('hcgw-mobile-menu-active');
        HcgwDockbar.mobileMenuOpen = true;

    },

    hideMobileMenu: function () {
        $('#hcgw-mobile-dropdown-menu').hide();
        $('#hcgw-mobile-menu-icon').removeClass('hcgw-mobile-menu-active');
        HcgwDockbar.mobileMenuOpen = false;
    }
};