var LandingPage = LandingPage || {
    MENU_ITEM_CLASS: 'landing-page-menu-item',
    EXPANDED_MENU_AREA_CLASS: 'landing-page-menu-item-expanded',
    MENU_ITEM_BUTTON_CLASS: 'landing-page-service-button',
    MENU_ITEM_PAGE_TITLE_CLASS: 'landing-menu-item-page-title',
    MENU_STATES: {open: 'OPEN', closed: 'CLOSED'},
    CIRCLE_CLASS: 'list-item-circle',

    Init: function () {

    },

    HomeLanding: {
        MENU_ID: 'home-landing-page',
        menuItemStates: {},
        numOrgs: 0,

        Init: function () {
            $(document).ready(function () {
                $('.' + LandingPage.CIRCLE_CLASS).hide();
            });
            LandingPage.HomeLanding.setupEventListeners();
        },

        setupEventListeners: function () {
            $(document).ready(function () {
                $('#' + LandingPage.HomeLanding.MENU_ID).on('click', '.' + LandingPage.MENU_ITEM_BUTTON_CLASS, function (e) {
                    e.stopPropagation();
                }).on('click', '.' + LandingPage.MENU_ITEM_PAGE_TITLE_CLASS, function (e) {
                    e.stopPropagation();
                }).on('click', '.' + LandingPage.MENU_ITEM_CLASS, function () {
                    LandingPage.HomeLanding.OnMenuItemClicked($(this));
                });

                $('.' + LandingPage.MENU_ITEM_PAGE_TITLE_CLASS).on({
                    mouseenter: function () {
                        $(this).siblings('.circle-holder').children('.list-item-circle').show();
                    },
                    mouseleave: function () {
                        $(this).siblings('.circle-holder').children('.list-item-circle').hide();
                    }
                })

                // If the there is only one organisation then
                // we automatically open this orgs menu.
                var $menus = $('.' + LandingPage.MENU_ITEM_CLASS);
                if($menus != null && $menus.length === 1) {
                    LandingPage.HomeLanding.OnMenuItemClicked($menus);
                }
            });
        },

        OnMenuItemClicked: function ($menu) {
            if(LandingPage.HomeLanding.isMenuOpen($menu)) {
                LandingPage.HomeLanding.closeMenu($menu);
            } else {
                LandingPage.HomeLanding.openMenu($menu);
            }
        },

        isMenuOpen: function ($menu) {
            return LandingPage.HomeLanding.menuItemStates[$menu.attr('id')] === LandingPage.MENU_STATES.open;
        },

        openMenu: function ($menu) {
            $menu.children('.' + LandingPage.EXPANDED_MENU_AREA_CLASS).slideDown(function () {
                LandingPage.HomeLanding.menuItemStates[$menu.attr('id')] = LandingPage.MENU_STATES.open;
            });
        },

        openMenuImmediate: function ($menu) {
            $menu.children('.' + LandingPage.EXPANDED_MENU_AREA_CLASS).show(function () {
                LandingPage.HomeLanding.menuItemStates[$menu.attr('id')] = LandingPage.MENU_STATES.open;
            });
        },

        closeMenu: function ($menu) {
            $menu.children('.' + LandingPage.EXPANDED_MENU_AREA_CLASS).slideUp(function () {
                LandingPage.HomeLanding.menuItemStates[$menu.attr('id')] = LandingPage.MENU_STATES.closed;
            });
        }
    },

    OrgLanding: {
        MENU_ID: 'org-landing-page',

        Init: function () {
            LandingPage.OrgLanding.setupEventListeners();
        },

        setupEventListeners: function () {
            /*$('#' + LandingPage.OrgLanding.MENU_ID).on()*/
        }
    }
};