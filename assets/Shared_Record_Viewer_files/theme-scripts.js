var ThemeScripts = ThemeScripts || {
    isIE7: false,
    dockbarShown: false,
    pagesMenuShown: false,

    Init: function () {
        $(document).ready(function () {
            if(navigator.appVersion.indexOf("MSIE 7.")!=-1) {
                ThemeScripts.isIE7 = true;
            }
            ThemeScripts.SetupEventListeners();

            $('#owl').children('.hoot').hide();
            $('#aw-yis').on('click', function () {
                ThemeScripts.moveOwl();
            });
        });
    },

    SetupEventListeners: function () {
        $('body').on('click', function () {

        });

        $('#hcgw-dockbar-menu-button').on('click', function () {
            if(!ThemeScripts.dockbarShown) {
                ThemeScripts.showDockbar();
            } else {
                ThemeScripts.hideDockbar();
            }
        });

        $('#hcgw-pages-menu-button').on('click', function () {
            if(!ThemeScripts.pagesMenuShown) {
                ThemeScripts.showPagesMenu();
            } else {
                ThemeScripts.hidePagesMenu();
            }
        });
    },

    showDockbar: function () {
        if(ThemeScripts.isIE7) {
            $('#dockbar-container').show();
            ThemeScripts.dockbarShown = true;
        } else {
            $('#hcgw-dockbar-menu-button').removeClass('menu-closed').addClass('menu-open');
            $('#dockbar-container').slideDown(function () {
                ThemeScripts.dockbarShown = true;
            });
        }
    },

    hideDockbar: function () {
        if(ThemeScripts.isIE7) {
            $('#dockbar-container').hide();
            ThemeScripts.dockbarShown = false;
        } else {
            $('#hcgw-dockbar-menu-button').removeClass('menu-open').addClass('menu-closed');
            $('#dockbar-container').slideUp(function () {
                ThemeScripts.dockbarShown = false;
            });
        }
    },

    showPagesMenu: function () {
        $('#hcgw-pages-menu-icon-icon').removeClass('icon-chevron-down').addClass('icon-chevron-left');
        $('#hcgw-pages-menu').slideDown(200, function () {
            ThemeScripts.pagesMenuShown = true;
            $(this).removeClass('hcgw-pages-menu-button-closed').addClass('hcgw-pages-menu-button-open');
        });
    },

    hidePagesMenu: function () {
        $('#hcgw-pages-menu-icon-icon').removeClass('icon-chevron-left').addClass('icon-chevron-down');
        $('#hcgw-pages-menu').slideUp(200, function () {
            ThemeScripts.pagesMenuShown = false;
            $(this).removeClass('hcgw-pages-menu-button-open').addClass('hcgw-pages-menu-button-closed');
        });
    },

    moveOwl: function () {
        $('#owl').animate({left: '500'}, 2000)
            .animate({height: '300', width: '300'}, 500, function () {
                $('#owl').children('.hoot').show();
                setTimeout(function () {
                    $('#owl').children('.hoot').hide();
                    $('#owl').animate({height: '200', width: '200'}, 500)
                        .animate({left: '+=2000'}, 2000).animate({left: "-300"}, 0);
                }, 500);
            });
    }
};

ThemeScripts.Init();