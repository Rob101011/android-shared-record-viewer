package com.example.sharedrecordviewer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class MainActivity extends Activity {
	
	WebView webview;

	ProgressDialog mProgressDialog;
	Handler progressHandler;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressDialog = new ProgressDialog(this);
        webview = (WebView) findViewById(R.id.webView);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.setInitialScale(1);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
//        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(false);
        settings.setSupportZoom(true);
        settings.setDefaultZoom(ZoomDensity.FAR);
        
        
        //attempt to optimise
        webview.getSettings().setRenderPriority(RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 19) {
        	webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        	
        }       
        else {
        	webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        } 
        
     
        webview.setWebViewClient(new CustomWebViewClient());
        webview.setVisibility(View.GONE);
        mProgressDialog.setTitle("Loading");
        mProgressDialog.show();
        mProgressDialog.setMessage("Welcome to the Shared Record Viewer");
        webview.loadUrl("http://srv.integration.healthcaregateway.uk/");
   //     webview.loadUrl("file:///android_asset/srv.html");
        
    }

	
	
//	private int getScale(){
//	    Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
//	    int width = display.getWidth(); 
//	    Double val = new Double(width)/new Double(PIC_WIDTH);
//	    val = val * 100d;
//	    return val.intValue();
//	}
	
	
	
	private void animate(final WebView view) {
        Animation anim = AnimationUtils.loadAnimation(getBaseContext(),
                android.R.anim.slide_in_left);
        view.startAnimation(anim);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	webview = (WebView) findViewById(R.id.webView);
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
        	
        	
        	
        	webview.setVisibility(View.GONE);
             mProgressDialog.setTitle("Loading");
             mProgressDialog.show();
             mProgressDialog.setMessage("Going back");
        	
        	webview.goBack();
            
            return true;
        }
        
        
        
        //---------------
        
        
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask the user if they want to quit
            new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.exit)
            .setMessage(R.string.really_exit)
            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //Stop the activity
                    MainActivity.this.finish();    
                }

            })
            .setNegativeButton(R.string.no, null)
            .show();

            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
       
    }


    
    // WebView Client Class

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.setVisibility(View.GONE);
            mProgressDialog.setTitle("Loading");
            mProgressDialog.show();
            mProgressDialog.setMessage("Loading " + url);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        	System.out.println("loaded " + url);
            mProgressDialog.dismiss();
            animate(view);
            view.setVisibility(View.VISIBLE);
            super.onPageFinished(view, urlTransform(url));
            
           
        }
        
        
        public String urlTransform(String url){
        	
        	if(url.equals("http://srv.integration.healthcaregateway.uk/web/guest/welcome"))
        		return "Welcome Page";
        	
        	if(url.equals("http://srv.integration.healthcaregateway.uk/group/integration-pct/patient-record"))
        		return "Patient Record";
        	
        	if(url.equals("http://srv.integration.healthcaregateway.uk/group/integration-pct/audit"))
        		return "Audit Records";
        	
        	if(url.equals("http://srv.integration.healthcaregateway.uk/group/integration-pct?"))
        		return "Home Page";
        	
        	return "Please wait";
        	
        	
        }
        
        
    }

    


}
