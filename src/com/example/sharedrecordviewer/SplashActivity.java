package com.example.sharedrecordviewer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

public class SplashActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        
        try
        { ((View) findViewById(android.R.id.title).getParent()).setVisibility(View.GONE);
        }
        catch (Exception e) {}
        

        int secondsDelayed = 3;
        new Handler().postDelayed(new Runnable() {
                public void run() {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                }
        }, secondsDelayed * 1000);
    }
}